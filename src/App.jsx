import "./App.css";
import { Routes, Route } from "react-router-dom";
import OwnRecipes from "./components/own_recipes/OwnRecipes";
import FavoriteRecipes from "./components/favorite_recipes/FavoriteRecipes";
import Home from "./components/Home";
import Layout from "./components/common/Layout";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="/" element={<Home />} />
          <Route path="/own" element={<OwnRecipes />} />
          <Route path="/favorite" element={<FavoriteRecipes />} />
        </Route>
      </Routes>
    </>
  );
}

export default App;
