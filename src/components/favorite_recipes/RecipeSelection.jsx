import React, { useEffect, useState } from "react";
import axios from "axios";
import ApiRecipeList from "./ApiRecipeList";
import styled from "styled-components";
import Error from "../common/Error";

const API_URL = "https://www.themealdb.com/api/json/v1/1/filter.php?c=";
const Container = styled.div`
  width: 60%;
`;
const Header = styled.div`
  margin-left: 5px;
  margin-right: 5px;
  padding: 10px 5px;
  display: flex;
  justify-content: space-between;
  gap: 20px;
  background-color: #f0f0f0;
  border-bottom: 1px solid #ccc;
  border-radius: 2px;
`;

const Select = styled.select`
  padding: 8px 12px;
  font-size: 16px;
  border: 1px solid #ccc;
  border-radius: 5px;
`;

const RecipeSelection = ({ addFavoriteRecipe }) => {
  const [meals, setMeals] = useState([]);
  const [error, setError] = useState();
  const [selectedCategory, setSelectedCategory] = useState("");

  const handleChange = (e) => {
    setSelectedCategory(e.target.value);
  };

  const fetchMeals = async () => {
    try {
      const response = await axios.get(API_URL + selectedCategory);
      setMeals(response?.data?.meals);
      setError(null);
    } catch (error) {
      setError(error);
    }
  };
  useEffect(() => {
    if (selectedCategory) {
      fetchMeals();
    }
  }, [selectedCategory]);

  return (
    <Container>
      <Header>
        <h3>Select Recipe</h3>
        <Select value={selectedCategory} onChange={handleChange}>
          <option value="" disabled>
            Select a category
          </option>
          <option value="beef">Beef</option>
          <option value="chicken">Chicken</option>
          <option value="pork">Pork</option>
          <option value="seafood">Sea Food</option>
        </Select>
      </Header>
      {error ? (
        <Error message={error.message} />
      ) : (
        <ApiRecipeList
          categorizedMeals={meals}
          addFavoriteRecipe={addFavoriteRecipe}
        />
      )}
    </Container>
  );
};

export default RecipeSelection;
