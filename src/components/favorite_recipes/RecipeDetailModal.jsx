import React, { useEffect, useMemo, useState } from "react";
import styled from "styled-components";
import axios from "axios";
import Error from "../common/Error";
import Ingredients from "./Ingredients";

const RECIPE_DETAIL_URL =
  "https://www.themealdb.com/api/json/v1/1/lookup.php?i=";

const ModalHeader = styled.div`
  background-color: #f0f0f0;
  color: #333;
  padding: 15px;
  border-radius: 10px;
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 10px;
`;

const Button = styled.button`
  background-color: ${(props) => props["bg-color"]};
  color: #fff;
  border: none;
  border-radius: 5px;
  font-size: 10px;
  cursor: pointer;
  margin: 5px;
  &:hover {
    background-color: ${(props) => props["hover-color"]};
  }
`;

const ModalBody = styled.div`
  display: flex;
  flex-direction: row;
`;

const SectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;
const SectionHeader = styled.div`
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 10px;
`;

const SectionBody = styled.div`
  text-align: left;
`;

const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 1000;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ModalContent = styled.div`
  background-color: white;
  padding: 20px;
  border-radius: 8px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.2);
  max-width: 60%;
  max-height: 60%;
  overflow: auto;
`;

const RecipeDetailModal = ({
  meal,
  toggleModal,
  addFavoriteRecipe,
  caller,
}) => {
  const [recipeDetail, setRecipeDetail] = useState({});
  const [error, setError] = useState();
  const [totalTime, setTotalTime] = useState();

  const fetchDetail = async () => {
    try {
      const response = await axios.get(RECIPE_DETAIL_URL + meal.idMeal);
      const mealDetail = response?.data?.meals?.[0];

      const instructions = mealDetail.strInstructions.split(/\r?\n/);
      const ingredients = [];
      for (let i = 1; i <= 20; i++) {
        const ingredient = mealDetail[`strIngredient${i}`];
        const measure = mealDetail[`strMeasure${i}`];
        if (!ingredient || !measure) break;
        ingredients.push(`${ingredient}, ${measure}`);
      }

      setRecipeDetail({
        title: mealDetail.strMeal,
        thumb: mealDetail.strMealThumb,
        instructions: instructions,
        ingredients: ingredients,
        rating: 0,
      });
      setError(null);
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    if (meal) {
      if (caller === "Selection") {
        fetchDetail();
      } else if (caller === "Saved") {
        //If this modal is called from SavedRecipeComponent which already has details,
        //Unneccessary to fetch again. Just assign it as it is.
        setRecipeDetail(meal);
      }
    }
  }, [meal]);

  useEffect(() => {
    if (recipeDetail && Object.keys(recipeDetail).length !== 0) {
      const totalTime = calculateTotalTime(recipeDetail);
      setTotalTime(totalTime);
    }
  }, [recipeDetail]);

  const handleAdd = async () => {
    addFavoriteRecipe(recipeDetail);
    toggleModal(null);
  };

  const calculateTotalTime = useMemo(() => {
    return (recipeDetail) => {
      let time = 0;
      const minuteRegex = /minute[s]?[,.]?/i;
      const hourRegex = /hour[s]?[,.]?/i;
      recipeDetail.instructions.forEach((line) => {
        const words = line.split(" ");

        words.forEach((word) => {
          const timeWordIndex = words.indexOf(word) - 1;
          const timeWord = words[timeWordIndex];
          if (word.match(minuteRegex)) {
            time += convertTimeString(timeWord);
          } else if (word.match(hourRegex)) {
            time += convertTimeString(timeWord) * 60;
          }
        });
      });

      return time;
    };
  }, [recipeDetail.instructions]);

  //Check if the numberWord string contains hyphen(-) which indicates time range
  const convertTimeString = (timeWord) => {
    if (isNaN(timeWord)) {
      const wordMap = {
        one: 1,
        two: 2,
        three: 3,
        four: 4,
        five: 5,
        six: 6,
        seven: 7,
        eight: 8,
        nine: 9,
        ten: 10,
      };
      if (timeWord.includes("-")) {
        // For now numberWord like "40-50"(range) would just be considered as "50"
        timeWord = timeWord.split("-")[1];
        return parseInt(timeWord);
      } else if (wordMap.hasOwnProperty(timeWord)) {
        return wordMap[timeWord.toLowerCase()];
      }
    }
    return parseInt(timeWord);
  };

  return (
    <>
      <ModalOverlay onClick={() => toggleModal(null)}>
        <ModalContent onClick={(e) => e.stopPropagation()}>
          {error ? (
            <Error message={error.message} />
          ) : (
            <div>
              <div>Total Cooking Time: {totalTime} minutes</div>
              <ModalHeader>
                <div>{recipeDetail.title}</div>
                {caller === "Selection" && (
                  <Button
                    bg-color="#ADD8E6"
                    hover-color="#0000FF"
                    onClick={handleAdd}
                  >
                    Add
                  </Button>
                )}
                <Button
                  bg-color="#FFB6C1"
                  hover-color="#FF0000"
                  onClick={() => toggleModal(null)}
                >
                  Close
                </Button>
              </ModalHeader>
              <ModalBody>
                <SectionContainer style={{ flex: 7 }}>
                  <SectionHeader>Instruction </SectionHeader>
                  <SectionBody>
                    {recipeDetail.instructions &&
                      recipeDetail.instructions.map((instruction, index) => (
                        <div key={index} style={{ marginBottom: "10px" }}>
                          {" "}
                          {instruction}{" "}
                        </div>
                      ))}
                  </SectionBody>
                </SectionContainer>
                <SectionContainer style={{ flex: 3 }}>
                  <SectionHeader>Ingredients </SectionHeader>
                  <SectionBody>
                    <Ingredients recipeDetail={recipeDetail} />
                  </SectionBody>
                </SectionContainer>
              </ModalBody>
            </div>
          )}
        </ModalContent>
      </ModalOverlay>
    </>
  );
};

export default RecipeDetailModal;
