import { useEffect, useState } from "react";
import axios from "axios";
import styled from "styled-components";

const RatingContainer = styled.div`
  margin-left: 5px;
  display: flex;
  flex-direction: row;
`;

const StarIcon = styled.span`
  cursor: pointer;
  color: ${(props) => props.color};
`;

const RadioInput = styled.input`
  display: none;
`;

const Rating = ({ index }) => {
  const [rating, setRating] = useState();
  const [hover, setHover] = useState(null);
  const [recipe, setRecipe] = useState();

  const fetch = async () => {
    const response = await axios.get("http://localhost:8000/recipes");
    const recipe = response.data[index];
    setRecipe(recipe);
    setRating(recipe.rating);
  };

  const updateRating = async () => {
    const ratedRecipe = { ...recipe, rating: rating };
    await axios.put(`http://localhost:8000/recipes/${recipe.id}`, ratedRecipe);
  };

  useEffect(() => {
    //Fetch recipe only first time
    if (!recipe) {
      fetch();
    }
    if (rating) {
      updateRating();
    }
  }, [rating]);

  return (
    <>
      <RatingContainer>
        {[...Array(5)].map((star, index) => {
          const currentRating = index + 1;
          return (
            <label key={index}>
              <RadioInput
                type="radio"
                name="rating"
                value={currentRating}
                onClick={() => setRating(currentRating)}
              />
              <StarIcon
                color={
                  currentRating <= (hover || rating) ? "#ffc107" : "#e4e5e9"
                }
                onMouseEnter={() => setHover(currentRating)}
                onMouseLeave={() => setHover(null)}
              >
                &#9733;
              </StarIcon>
            </label>
          );
        })}
      </RatingContainer>
    </>
  );
};

export default Rating;
