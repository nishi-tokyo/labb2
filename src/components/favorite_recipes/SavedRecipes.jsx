import styled from "styled-components";
import RecipeDetailModal from "./RecipeDetailModal";
import { useState } from "react";
import axios from "axios";
import Rating from "./Rating";

const Header = styled.div`
  padding: 10px 5px;
  background-color: #f0f0f0;
  border-bottom: 1px solid #ccc;
  border-radius: 2px;
`;

const Card = styled.div`
  background-color: #ffffff;
  border-radius: 8px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  margin-bottom: 10px;
  padding: 10px;
  &:hover {
    box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.2);
    cursor: pointer;
  }
`;
const Container = styled.div`
  display: flex;
  flex-direction: column;
`;
const CardBody = styled.div`
  display: flex;
`;
const CardHeader = styled.div`
  text-align: left;
`;
const ButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-start; 
`;

const Button = styled.button`
  background-color: ${(props) => props.bgcolor};
  color: #fff;
  border: none;
  border-radius: 5px;
  font-size: 10px;
  margin-left: 5px;
  margin-bottom: 5px;
  cursor: pointer;
  &:hover {
    background-color: ${(props) => props.hovercolor};
  }
`;

const SavedRecipes = ({ favoriteRecipes, removeFavoriteRecipe }) => {
  const [showModal, setShowModal] = useState(false);
  const [chosenRecipe, setChosenRecipe] = useState({});
  const handleDetailClick = (recipe) => {
    setChosenRecipe(recipe);
    setShowModal(true);
  };
  const toggleModal = () => {
    setShowModal(!showModal);
  };
  const handleRemove = async (index) => {
    const isConfirmed = window.confirm(
      "Are you sure you want to remove this item?",
    );
    if (isConfirmed) {
      const response = await axios.get("http://localhost:8000/recipes");
      const recipeToDelete = response.data[index];
      await axios.delete(`http://localhost:8000/recipes/${recipeToDelete.id}`);
      removeFavoriteRecipe(index);
    }
  };
  return (
    <>
      <Container>
        <Header>
          <h3>Saved Recipes</h3>
        </Header>
        {favoriteRecipes &&
          favoriteRecipes.map((recipe, index) => (
            <Card key={index}>
              <CardHeader>{recipe.title}</CardHeader>
              <CardBody>
                <img src={recipe.thumb} style={{ width: "100px" }}></img>
                <ButtonContainer>
                  <Button
                    bgcolor="#ADD8E6"
                    hovercolor="#0000FF"
                    onClick={() => handleDetailClick(recipe)}
                  >
                    Details
                  </Button>
                  <Button
                    bgcolor="#FFB6C1"
                    hovercolor="#FF0000"
                    onClick={() => handleRemove(index)}
                  >
                    Remove
                  </Button>
                  <Rating index={index} />
                </ButtonContainer>
              </CardBody>
            </Card>
          ))}
        {showModal && (
          <RecipeDetailModal
            meal={chosenRecipe}
            toggleModal={toggleModal}
            caller="Saved"
          />
        )}
      </Container>
    </>
  );
};

export default SavedRecipes;
