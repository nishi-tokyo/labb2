import { useCallback, useEffect, useState } from "react";
import SavedRecipes from "./SavedRecipes";
import RecipeSelection from "./RecipeSelection";
import styled from "styled-components";
import axios from "axios";

const Container = styled.div`
  display: flex;
  justify-content: space-between;
`;

const FavoriteRecipes = () => {
  const [favoriteRecipes, setFavoriteRecipes] = useState([]);

  //useCallback to avoid reinstanciating, as this functions is drilled down to its child components.
  //Reinstancites only when favoriteRecipes change.
  const addFavoriteRecipe = useCallback(
    async (recipe) => {
      const titles = favoriteRecipes.map((r) => r.title);
      if (titles.includes(recipe.title)) {
        window.alert("You have already saved this recipe.");
        return;
      }
      await axios.post("http://localhost:8000/recipes", JSON.stringify(recipe));
      const newFaviroteRecipes = [...favoriteRecipes, recipe];
      setFavoriteRecipes(newFaviroteRecipes);
    },
    [favoriteRecipes],
  );

  const removeFavoriteRecipe = (indexToRemove) => {
    const updatedRecipes = favoriteRecipes.filter(
      (_, index) => index !== indexToRemove,
    );
    setFavoriteRecipes(updatedRecipes);
  };

  const fetchData = async () => {
    const response = await axios.get("http://localhost:8000/recipes");
    setFavoriteRecipes(response.data);
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Container>
      <RecipeSelection addFavoriteRecipe={addFavoriteRecipe} />
      <SavedRecipes
        favoriteRecipes={favoriteRecipes}
        removeFavoriteRecipe={removeFavoriteRecipe}
      />
    </Container>
  );
};

export default FavoriteRecipes;
