import styled from "styled-components";
import RecipeDetailModal from "./RecipeDetailModal";
import { useState } from "react";

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  & > div {
    margin: 5px;
  }
`;

const Card = styled.div`
  background-color: #ffffff;
  border-radius: 8px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  padding: 8px;
  width: calc(50% - 10px); 
  &:hover {
    box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.2);
    cursor: pointer;
  }
  display: flex;
  flex-direction: column;
  flex-start; 
`;
const ClickableText = styled.div`
  text-align: left;
  &:hover {
    color: #2e8b57;
  }
`;
const Img = styled.img`
  width: 120px;
`;

const ApiRecipeList = ({ categorizedMeals, addFavoriteRecipe }) => {
  const [showModal, setShowModal] = useState(false);
  const [meal, setMeal] = useState({});
  const toggleModal = (meal) => {
    setMeal(meal);
    setShowModal(!showModal);
  };
  return (
    <>
      <Container>
        {categorizedMeals &&
          categorizedMeals.map((meal, index) => (
            <Card key={index} onClick={() => toggleModal(meal)}>
              <ClickableText>{meal.strMeal}</ClickableText>
              <Img src={meal.strMealThumb} />
            </Card>
          ))}
        {showModal && (
          <RecipeDetailModal
            meal={meal}
            toggleModal={toggleModal}
            addFavoriteRecipe={addFavoriteRecipe}
            caller="Selection"
          />
        )}
      </Container>
    </>
  );
};

export default ApiRecipeList;
