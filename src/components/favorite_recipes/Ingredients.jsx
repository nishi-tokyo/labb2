import { useEffect, useState } from "react";

const Ingredients = ({ recipeDetail }) => {
  const [ingredients, setIngredients] = useState([]);
  const [sorted, setSorted] = useState(false);

  const handleSort = () => {
    const ingredients = sorted
      ? [...recipeDetail.ingredients].sort()
      : recipeDetail.ingredients;

    setIngredients(ingredients);
    setSorted(!sorted);
  };
  useEffect(() => {
    setIngredients(recipeDetail.ingredients);
  }, [recipeDetail.ingredients]);

  return (
    <>
      <button onClick={handleSort}>Sort</button>
      <div>
        {ingredients &&
          ingredients.map((ingredient, index) => (
            <div key={index} style={{ marginBottom: "5px" }}>
              {" "}
              {ingredient}{" "}
            </div>
          ))}
      </div>
    </>
  );
};

export default Ingredients;
