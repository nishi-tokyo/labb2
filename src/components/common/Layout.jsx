import { Link, Outlet } from "react-router-dom";
import styled from "styled-components";

const Nav = styled.nav`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  background: #bad5ff;
`;

const Ul = styled.ul`
  display: flex;
  justify-content: center;
  list-style-type: none;
  padding: 0;
`;

const Li = styled.li`
  margin: 10px;
`;

const OutletContainer = styled.div`
  margin-top: 50px;
`;
const Layout = () => {
  return (
    <>
      <Nav>
        <Ul>
          <Li>
            <Link to="/">Home</Link>
          </Li>
          <Li>
            <Link to="/own">Own recipes</Link>
          </Li>
          <Li>
            <Link to="/favorite">Favorite recipes</Link>
          </Li>
        </Ul>
      </Nav>
      <OutletContainer>
        <Outlet />
      </OutletContainer>
    </>
  );
};

export default Layout;
