import styled from "styled-components";

const ErrorContainer = styled.div`
  margin-left: 5px;
  margin-right: 5px;
  padding: 20px;
  background-color: #f8d7da;
  color: #721c24;
  border: 1px solid #f5c6cb;
`;
const Error = ({ message }) => {
  return (
    <>
      <ErrorContainer>
        <p>Error: {message}</p>
      </ErrorContainer>
    </>
  );
};

export default Error;
