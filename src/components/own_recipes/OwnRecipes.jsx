import React, { createContext, useEffect, useState } from "react";
import RecipeList from "./RecipeList";
import NavBar from "./NavBar";
import Footer from "./Footer";
import Modal from "./Modal";

export const ModalContext = createContext({});

const OwnRecipes = () => {
  const [recipeList, setRecipeList] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("../initRecipe.json");
      const data = await response.json();
      setRecipeList(data);
    };
    fetchData();
  }, []);

  const [showModal, setShowModal] = useState(false);
  const [modalType, setModalType] = useState("");
  const value = {
    showModal,
    setShowModal,
    modalType,
    setModalType,
  };

  const addRecipe = (recipe) => {
    const newRecipeList = [...recipeList, recipe];
    setRecipeList(newRecipeList);
  };

  const [targetIndex, setTargetIndex] = useState();
  const editRecipe = (editedRecipe) => {
    const newRecipeList = [...recipeList];
    newRecipeList[targetIndex] = editedRecipe;
    setRecipeList(newRecipeList);
  };

  const deleteRecipe = (index) => {
    const newRecipeList = [
      ...recipeList.slice(0, index),
      ...recipeList.slice(index + 1),
    ];
    setRecipeList(newRecipeList);
  };

  return (
    <>
      <ModalContext.Provider value={value}>
        <NavBar setTargetIndex={setTargetIndex} />
        <RecipeList
          recipeList={recipeList}
          setTargetIndex={setTargetIndex}
          deleteRecipe={deleteRecipe}
        />
        {showModal && (
          <Modal
            index={targetIndex}
            recipeList={recipeList}
            setShowModal={setShowModal}
            modalType={modalType}
            addRecipe={addRecipe}
            editRecipe={editRecipe}
          />
        )}
        <Footer recipeList={recipeList} />
      </ModalContext.Provider>
    </>
  );
};

export default OwnRecipes;
