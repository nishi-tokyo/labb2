import React from "react";
import RecipeCard from "./RecipeCard";

const RecipeList = ({ recipeList, setTargetIndex, deleteRecipe }) => {
  return (
    <>
      {recipeList.map((recipe, index) => (
        <div key={index} className="mt-3">
          <RecipeCard
            index={index}
            recipe={recipe}
            setTargetIndex={setTargetIndex}
            deleteRecipe={deleteRecipe}
          />
        </div>
      ))}
    </>
  );
};

export default RecipeList;
